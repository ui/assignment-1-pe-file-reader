/// @file
/// @brief Contains structures for storing PE file headers and PE file reading functions definitions

#pragma once
#ifndef _PE_READ_H_
/// @brief Include guard
#define _PE_READ_H_

/// @brief Offset to the location of pointer to PE header in PE file 
#define MAIN_OFFSET 0x3c

/// @brief Max length of section name in PE file
#define MAX_SECTION_NAME_LENGTH 8

#ifdef _MSC_VER

typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;

#else
#include <stdint.h>
#endif

/*! \struct PEHeader

    This structure stores data of PE header section in PE file
*/

#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __GNUC__
    __attribute__((packed))
#endif
    PEHeader
{
    /// @brief The signature that identifies the file as a PE format image file
    uint8_t magic[4];
    /// @brief The number that identifies the type of target machine
    uint16_t Machine;
    /// @brief The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t NumberOfSections;
    /// @brief The low 32 bits of the number of seconds since 00:00 January 1, which indicates when the file was created
    uint32_t TimeDateStamp;
    /// @brief The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    uint32_t PointerToSymbolTable;
    /// @brief The number of entries in the symbol table
    uint32_t NumberOfSymbols;
    /// @brief The size of the optional header
    uint16_t SizeOfOptionalHeader;
    /// @brief The flags that indicate the attributes of the file
    uint16_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

/*! \struct SectionHeader

    This structure stores data of one section header in PE file
*/

#if defined _MSC_VER
#pragma pack(push, 1)
#endif
struct
#if defined __GNUC__
    __attribute__((packed))
#endif
    SectionHeader
{
    /// @brief The name of the section
    uint8_t Name[8];
    /// @brief The total size of the section when loaded into memory
    uint32_t VirtualSize;
    /// @brief The address of the first byte of the section relative to the image base when the section is loaded into memory
    uint32_t VirtualAddres;
    /// @brief The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t SizeOfRawData;
    /// @brief The file pointer to the first page of the section within the COFF file
    uint32_t PointerToRawData;
    /// @brief The file pointer to the beginning of relocation entries for the section
    uint32_t PointerToRelocations;
    /// @brief The file pointer to the beginning of line-number entries for the section
    uint32_t PointerToLinenumbers;
    /// @brief The number of relocation entries for the section
    uint16_t NumberOfRelocations;
    /// @brief The number of line-number entries for the section
    uint16_t NumberOfLinenumbers;
    /// @brief The flags that describe the characteristics of the section
    uint32_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

int seek_pe_header(FILE* stream);
int read_pe_header(FILE* stream, struct PEHeader* header);
int read_section_header(FILE* stream, struct SectionHeader* section);
int copy_pe_section(FILE* source, char* section_name, FILE* destination);

#endif
