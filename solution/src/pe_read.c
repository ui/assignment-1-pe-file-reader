/// @file
/// @brief Contains functions for reading PE file headers

#include <malloc.h>
#include <stdio.h>
#include <string.h>

#include "pe_read.h"

/// @brief Move the file pointer to location of PE header
/// @param stream PE file to move file pointer in
/// @return 0 in case of succes or error code
int seek_pe_header(FILE* stream){
    int result = fseek(stream, MAIN_OFFSET, SEEK_SET);
    if(result){
        return 1;
    }

    uint32_t offset;
    size_t size = fread(&offset, 4, 1, stream);
    if(size != 1){
        return 1;
    }

    result = fseek(stream, offset, SEEK_SET);
    if(result){
        return 1;
    }
    
    return 0;
}

/// @brief Read PE header section of PE file
/// @param stream File to read from
/// @param header Struct to read into
/// @return 0 in case of succes or error code
int read_pe_header(FILE* stream, struct PEHeader* header){
    size_t size = fread(header, sizeof(struct PEHeader), 1, stream);
    
    if(size != 1){
        return 1;
    }

    return 0;
}

/// @brief Read section header of one section in PE file
/// @param stream File to read from
/// @param section Struct to read into
/// @return 0 in case of succes or error code
int read_section_header(FILE* stream, struct SectionHeader* section){
    size_t size = fread(section, sizeof(struct SectionHeader), 1, stream);
    if(size != 1){
        return 1;
    }

    return 0;
}

/// @brief Compute length of section name in PE file
/// @param name Section name to compute length of
/// @return Length of section name
size_t section_name_length(uint8_t* name){
    for(size_t len = 0; len < MAX_SECTION_NAME_LENGTH; len++){
        if(name[len] == '\0'){
            return len;
        }
    }

    return MAX_SECTION_NAME_LENGTH;
}

/// @brief Compare name to PE section name 
/// @param name_1 Name to compare with
/// @param name_2 PE section name to compare to
/// @return 0 if names are equal and 1 if they are not
int compare_section_names(char* name_1, uint8_t* name_2){
    size_t len_1 = strlen(name_1);
    size_t len_2 = section_name_length(name_2);

    if(len_1 != len_2){
        return 1;
    }

    if(memcmp(name_1, name_2, len_1) != 0){
        return 1;
    }

    return 0;
}

/// @brief Copy part of one file into another
/// @param source File co copy from
/// @param destination File to copy into
/// @param amount Amount of bits to copy
/// @return 0 in case of succes or error code
int copy_file_section(FILE* source, FILE* destination, size_t amount){
    uint8_t buffer;
    size_t st;
    for (size_t i = 0; i < amount; i += sizeof(buffer)){
        st = fread(&buffer, sizeof(buffer), 1, source);
        if(st){
            fwrite(&buffer, sizeof(buffer), 1, destination);
        }else{
            return 1;
        }
    }

    return 0;
}

/// @brief Copy raw data of section in PE file into another file 
/// @param source PE file to copy from
/// @param section_name Name of section in PE file
/// @param destination File to copy into
/// @return 0 in case of succes or error code
int copy_pe_section(FILE* source, char* section_name, FILE* destination){
    if(seek_pe_header(source)){
        return 1;
    }

    struct PEHeader header;
    if(read_pe_header(source, &header)){
        return 1;
    }

    int result = fseek(source, header.SizeOfOptionalHeader, SEEK_CUR);
    if(result){
        return 1;
    }

    struct SectionHeader section;
    size_t i;
    for(i = 0; i < header.NumberOfSections; i++){
        if(read_section_header(source, &section)){
            return 1;
        }

        if(compare_section_names(section_name, section.Name) == 0){
            break;
        }
    }
    if(i == header.NumberOfSections){
        return 1;
    }

    result = fseek(source, section.PointerToRawData, SEEK_SET);
    if(result){
        return 1;
    }

    result = copy_file_section(source, destination, section.SizeOfRawData);
    if(result){
        return 1;
    }

    return 0;
}
